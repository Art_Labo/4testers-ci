import os

import requests


def test_check_weather_in_a_city():
    latitude = os.getenv("CITY_LATITUDE", 51.62574)
    longitude = os.getenv("CITY_LONGITUDE", 21.93274)
    weather_url = "https://api.open-meteo.com/v1/forecast"
    params = {"latitude": latitude, "longitude": longitude}
    get_weather_for_city_response = requests.get(weather_url, params=params)
    assert get_weather_for_city_response.status_code == 200
